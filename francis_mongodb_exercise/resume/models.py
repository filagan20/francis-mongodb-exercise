from djongo import models
from django import forms


class Certification(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Education(models.Model):
    start_year = models.PositiveIntegerField()
    end_year = models.PositiveIntegerField()
    attainment = models.CharField(max_length=100)
    course = models.CharField(max_length=100)
    school = models.CharField(max_length=100)
    certifications = models.ArrayModelField(
        model_container=Certification,
    )


class Experience(models.Model):
    start_year = models.PositiveIntegerField()
    end_year = models.PositiveIntegerField()
    current_job = models.BooleanField()
    company_name = models.CharField(max_length=100)
    company_address = models.CharField(max_length=100)
    description = models.CharField(max_length=500)


class Expertise(models.Model):
    name = models.CharField(max_length=100)
    rating = models.PositiveIntegerField(default=1)


class Skill(models.Model):
    name = models.CharField(max_length=100)
    rating = models.PositiveIntegerField(default=1)


class Interest(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Hobby(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Proficiency(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Person(models.Model):
    _id = models.ObjectIdField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    career_objective = models.CharField(max_length=500)
    birthday = models.DateField()
    birth_place = models.CharField(max_length=100)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=100, choices=GENDER_CHOICES)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=100)
    email = models.EmailField()

    interests = models.ArrayModelField(
        model_container=Interest,
    )
    hobbies = models.ArrayModelField(
        model_container=Hobby,
    )
    proficiencies = models.ArrayModelField(
        model_container=Proficiency,
    )

    education = models.ArrayModelField(
        model_container=Education,
    )
    experience = models.ArrayModelField(
        model_container=Experience,
    )
    expertise = models.ArrayModelField(
        model_container=Expertise,
    )
    skill = models.ArrayModelField(
        model_container=Skill,
    )

    objects = models.DjongoManager()

    def __str__(self):
        full_name = self.first_name + ' ' + self.last_name
        return full_name
