from django.core.exceptions import ObjectDoesNotExist, ValidationError

from .models import Person
from .serializers import (
    PersonSerializer, ExperienceSerializer, EducationSerializer,
    ProficiencySerializer, ContactSerializer, PersonWithSkillSerializer
)
import json


def get_record(person):
    serializer = PersonSerializer(person)
    return {
        'status_code': 200,
        'message': 'Record Found',
        'data': serializer.data
    }


def get_person_work(person):
    object_list = person.experience
    serializer = ExperienceSerializer(object_list, many=True)
    return {
        'status_code': 200,
        'message': 'List Returned',
        'data': serializer.data
    }


def get_person_education(person):
    object_list = person.education
    serializer = EducationSerializer(object_list, many=True)
    return {
        'status_code': 200,
        'message': 'List Returned',
        'data': serializer.data
    }


def get_person_tech_proficiencies(person):
    object_list = person.proficiencies
    serializer = ProficiencySerializer(object_list, many=True)
    return {
        'status_code': 200,
        'message': 'List Returned',
        'data': serializer.data
    }


def get_person_contact(person):
    serializer = ContactSerializer(person)
    return {
        'status_code': 200,
        'message': 'Details Returned',
        'data': serializer.data
    }


class PersonActionHandler(object):
    actions = {
        "get_person": get_record,
        "get_person_work": get_person_work,
        "get_person_education": get_person_education,
        "get_person_tech_proficiencies": get_person_tech_proficiencies,
        "get_person_contact": get_person_contact,
    }

    @classmethod
    def run_action(self, request):
        if "pk" in list(request.keys()):
            req_pk = request['pk']
            req_action = request['action']
            try:
                person = Person.objects.get(pk=req_pk)
                message = self.actions[req_action](person)
            except ObjectDoesNotExist:
                message = {
                    'status_code': 404,
                    'message': 'Record not Found'
                }
        else:
            message = {
                'status_code': 400,
                'message': 'Bad Request',
                'errors': {
                    'pk': 'This field is required'
                }
            }
        return message


class SkillActionHandler(object):
    @classmethod
    def run_action(self, request):
        if "skill" in list(request.keys()):
            proficiency = request['skill']
            person_list = Person.objects.filter(
                proficiencies={'name': proficiency})
            serializer = PersonWithSkillSerializer(person_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        else:
            message = {
                'status_code': 400,
                'message': 'Bad Request',
                'errors': {
                    'skill': 'This field is required'
                }
            }
        return message
