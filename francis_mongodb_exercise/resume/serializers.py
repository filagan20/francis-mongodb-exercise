from rest_framework import serializers
from .models import (
    Person, Education, Experience,
    Expertise, Skill, Certification,
    Interest, Hobby, Proficiency
)
from bson import ObjectId


class CertificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Certification
        exclude = ('id',)


class EducationSerializer(serializers.ModelSerializer):
    certifications = CertificationSerializer(many=True)

    class Meta:
        model = Education
        exclude = ('id',)


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        exclude = ('id',)


class ExpertiseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expertise
        exclude = ('id',)


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        exclude = ('id',)


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        exclude = ('id',)


class HobbySerializer(serializers.ModelSerializer):
    class Meta:
        model = Hobby
        exclude = ('id',)


class ProficiencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Proficiency
        exclude = ('id',)

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('address', 'phone', 'email')

class PersonWithSkillSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    class Meta:
        model = Person
        fields = ('id', 'first_name', 'last_name')

    def get_id(self, obj):
        return str(obj._id)

class PersonSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()

    education = EducationSerializer(many=True)
    experience = ExperienceSerializer(many=True)
    expertise = ExpertiseSerializer(many=True)
    skill = SkillSerializer(many=True)

    interests = InterestSerializer(many=True)
    hobbies = HobbySerializer(many=True)
    proficiencies = ProficiencySerializer(many=True)

    class Meta:
        model = Person
        exclude = ('_id',)

    def get_id(self, obj):
        return str(obj._id)