from channels.generic.websocket import AsyncWebsocketConsumer
from .api_actions import PersonActionHandler, SkillActionHandler
import json


class ResumeConsumer(AsyncWebsocketConsumer):
    actions = {
        "get_person": PersonActionHandler,
        "get_person_work": PersonActionHandler,
        "get_person_education": PersonActionHandler,
        "get_person_tech_proficiencies": PersonActionHandler,
        "get_person_contact": PersonActionHandler,
        "get_person_with_skill": SkillActionHandler,
    }

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        request_json = json.loads(text_data)
        if "action" in list(request_json.keys()):
            req_action = request_json['action']

            if req_action in list(self.actions.keys()):
                resp_message = self.actions[req_action].run_action(request_json)
            else:
                resp_message = {
                    'status_code': 400,
                    'message': 'Bad Request',
                    'errors': {
                        'action': 'Action does not exist'
                    }
                }
        else:
            resp_message = {
                'status_code': 400,
                'message': 'Bad Request',
                'errors': {
                    'action': 'This field is required'
                }
            }
       
        await self.send(text_data=json.dumps(resp_message))
